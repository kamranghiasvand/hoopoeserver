﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Protocol.Stack
{
    /// <summary>
    /// Specifies BindInfo protocol.
    /// </summary>
    public enum BindInfoProtocol
    {
        /// <summary>
        /// TCP protocol.
        /// </summary>
        TCP,

        /// <summary>
        /// UDP protocol.
        /// </summary>
        UDP
    }
}
