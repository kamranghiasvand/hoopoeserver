﻿using Server.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Protocol.Stack
{
    /// <summary>
    /// Represents the method that will handle the SIP_Stack.Error event.
    /// </summary>
    /// <param name="x">Exception happened.</param>
    public delegate void SIP_ErrorEventHandler(Exception x);

    public class SIP_Stack
    {
        private BindInfo[] m_pBinds = null;
        private SIP_TransportLayer m_pTransportLayer;
        private Logger m_pLogger;
        private int m_MaximumConnections = 0;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SIP_Stack()
        {
            //  m_pCore = new SIP_ProxyCore(this);
            m_pTransportLayer = new SIP_TransportLayer(this);
            //m_pTransactionLayer = new SIP_TransactionLayer(this);
            //m_pNonceManager = new Auth_HttpDigest_NonceManager();

            m_pBinds = new BindInfo[] { };
            m_pLogger = new Logger();
        }


        /// <summary>
        /// Gets or sets socket bind info. Use this property to specify on which protocol,IP,port server 
        /// listnes and also if connections is SSL.
        /// </summary>
        public BindInfo[] BindInfo
        {
            get { return m_pBinds; }

            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("BindInfo");
                }

                //--- See binds has changed --------------
                bool changed = false;
                if (m_pBinds.Length != value.Length)
                {
                    changed = true;
                }
                else
                {
                    for (int i = 0; i < m_pBinds.Length; i++)
                    {
                        if (!m_pBinds[i].Equals(value[i]))
                        {
                            changed = true;
                            break;
                        }
                    }
                }

                if (changed)
                {
                    // If server is currently running, stop it before applying bind info.
                    bool running = m_pTransportLayer.IsRunning;
                    if (running)
                    {
                        m_pTransportLayer.Stop();
                        System.Threading.Thread.Sleep(1000);
                    }

                    m_pBinds = value;

                    // We need to restart server to take effect IP or Port change
                    if (running)
                    {
                        m_pTransportLayer.Start();
                    }
                }
            }
        }

        /// <summary>
        /// This event is raised by any SIP element when unknown error happened.
        /// </summary>
        public event SIP_ErrorEventHandler Error = null;

        #region method OnError

        /// <summary>
        /// Is called when ever unknown error happens.
        /// </summary>
        /// <param name="x">Exception happened.</param>
        internal void OnError(Exception x)
        {
            if (this.Error != null)
            {
                this.Error(x);
            }
        }

        #endregion

        /// <summary>
        /// Gets SIP logger.
        /// </summary>
        public Logger Logger
        {
            get { return m_pLogger; }
        }

        /// <summary>
        /// Gets or sets how many cunncurent connections allowed. Value 0 means not limited. This is used only for TCP based connections.
        /// </summary>
        public int MaximumConnections
        {
            get { return m_MaximumConnections; }

            set
            {
                if (value < 1)
                {
                    m_MaximumConnections = 0;
                }
                else
                {
                    m_MaximumConnections = value;
                }
            }
        }

    }
}