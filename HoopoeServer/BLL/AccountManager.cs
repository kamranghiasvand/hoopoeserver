﻿using HoopoeServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HoopoeServer.BLL
{

    public class AccountManager
    {

        DataModelContainer container = null;
        static AccountManager instance = null;

        bool isRunning = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        /// <exception cref="AccountManagerException"></exception>
        private AccountManager(DataModelContainer container)
        {
            if (container == null)
                throw new AccountManagerException("Container is null");
            this.container = container;
        }
        private AccountManager()
        {
            container = new DataModelContainer();
        }

        public static AccountManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AccountManager();
                }
                return instance;
            }
        }
        public void AddUser(Profile myProfile, string addressOfRecord, String userName, String password)
        {
            if (this.container == null)
                throw new AccountManagerException("container is null");
            Account ac = new Account();
            var existAccount = this.container.Accounts.Where(s => s.UserName == userName && s.AddressOfRecord == addressOfRecord).FirstOrDefault();
            if (existAccount != null)
                throw new AccountManagerException(String.Format("an account with username={0} and addressOfRecord={1} is exist. failed to add new account", userName, password));
            ac.Password = password;
            ac.AddressOfRecord = addressOfRecord;
            ac.UserName = userName;
            ac.MyProfile = myProfile;
            this.container.Accounts.Add(ac);
            container.SaveChanges();
        }
        public bool Exist(string addressOfRecord, string userName)
        {
            if (container == null)
                throw new AccountManagerException("container is null");
            var sel = (from account in container.Accounts
                       where account.AddressOfRecord == addressOfRecord
                       && account.UserName == userName
                       select account).FirstOrDefault();
            if (sel != null)
            {
                return true;
            }
            return false;
        }
        public void CahngeStatus(string addressOfRecord, AccountStatus status)
        {
            if (container == null)
                throw new AccountManagerException("container is null");

            var ac = (from account in container.Accounts
                      where account.AddressOfRecord == addressOfRecord
                      select account).FirstOrDefault();
            if (ac == null)
            {
                throw new AccountManagerException(String.Format("account with address={0} is not exists", addressOfRecord));
            }
            ac.Status = status;
            container.Entry(ac).State = System.Data.Entity.EntityState.Modified;
            container.SaveChanges();

        }
        public bool Athenticate(string userName, string password, string addressOfRecord)
        {
            if (container == null)
            {
                throw new AccountManagerException("container is null");
            }
            var address = (from account in container.Accounts
                           where account.AddressOfRecord == addressOfRecord && account.UserName == userName
                           && account.Password == password
                           select account).FirstOrDefault();
            if (address != null)
            {
                return true;
            }
            return false;
        }





    }
}
