﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoopoeServer.BLL
{
    [Serializable]
    public class AccountManagerException : Exception
    {
        public AccountManagerException() { }
        public AccountManagerException(string message) : base(message) { }
        public AccountManagerException(string message, Exception inner) : base(message, inner) { }
        protected AccountManagerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
