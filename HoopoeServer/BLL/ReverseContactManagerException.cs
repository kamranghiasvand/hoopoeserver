﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoopoeServer.BLL
{
    [Serializable]
    public class ReverseContactManagerException : Exception
    {
        public ReverseContactManagerException() { }
        public ReverseContactManagerException(string message) : base(message) { }
        public ReverseContactManagerException(string message, Exception inner) : base(message, inner) { }
        protected ReverseContactManagerException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
