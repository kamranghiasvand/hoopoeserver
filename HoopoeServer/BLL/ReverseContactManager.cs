﻿using HoopoeServer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoopoeServer.BLL
{
    public class ReverseContactManager
    {
        ReverseContactManager instance = null;
        DataModelContainer container;
        public ReverseContactManager(DataModelContainer container)
        {
            if (container == null)
                throw new ReverseContactManagerException("container can not be null");
            this.container = container;
        }
        public ReverseContactManager()
        {
            this.container = new DataModelContainer();
        }
        public ReverseContactManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new ReverseContactManager();
                return instance;
            }
        }
        public void SendStatusToFriends(string accountId, AccountStatus status)
        {
            if (container == null)
            {
                throw new ReverseContactManagerException("container can not be null");
            }
            var friends = (from friend in container.ReverseContacts
                           where friend.MeId == accountId
                           select friend);
            foreach (var friend in friends)
            {
                sendAccontStatusToFriend(accountId, status, friend.WhoIAmFrindId);
            }
        }
        private void sendAccontStatusToFriend(string accountId, AccountStatus status, String friendId)
        {

        }

    }
}
