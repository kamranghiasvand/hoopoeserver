//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HoopoeServer.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Event
    {
        public string Id { get; set; }
        public System.DateTime OccurDate { get; set; }
        public EventType Type { get; set; }
        public string ConversationId { get; set; }
        public string ContentId { get; set; }
        public string Description { get; set; }
        public int NumberOfSeen { get; set; }
        public string Caption { get; set; }
    
        public virtual Conversation Conversation { get; set; }
        public virtual Content Contents { get; set; }
    }
}
