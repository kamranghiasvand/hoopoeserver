
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/01/2015 12:06:57
-- Generated from EDMX file: G:\bitbucket\HoopoeServer\HoopoeServer\Model\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HoopeoServer];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_MyProfileAccountInfo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [FK_MyProfileAccountInfo];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountConversation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Conversations] DROP CONSTRAINT [FK_AccountConversation];
GO
IF OBJECT_ID(N'[dbo].[FK_ConversationRecipient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Recipients] DROP CONSTRAINT [FK_ConversationRecipient];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountRecipient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Recipients] DROP CONSTRAINT [FK_AccountRecipient];
GO
IF OBJECT_ID(N'[dbo].[FK_ConversationEvent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_ConversationEvent];
GO
IF OBJECT_ID(N'[dbo].[FK_EventContent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Contents] DROP CONSTRAINT [FK_EventContent];
GO
IF OBJECT_ID(N'[dbo].[FK_Call_inherits_Event]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events_Call] DROP CONSTRAINT [FK_Call_inherits_Event];
GO
IF OBJECT_ID(N'[dbo].[FK_Message_inherits_Event]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events_Message] DROP CONSTRAINT [FK_Message_inherits_Event];
GO
IF OBJECT_ID(N'[dbo].[FK_Birthday_inherits_Event]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events_Birthday] DROP CONSTRAINT [FK_Birthday_inherits_Event];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Profiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Profiles];
GO
IF OBJECT_ID(N'[dbo].[Accounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accounts];
GO
IF OBJECT_ID(N'[dbo].[Conversations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Conversations];
GO
IF OBJECT_ID(N'[dbo].[Recipients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Recipients];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[Contents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contents];
GO
IF OBJECT_ID(N'[dbo].[ReverseContacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ReverseContacts];
GO
IF OBJECT_ID(N'[dbo].[Events_Call]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events_Call];
GO
IF OBJECT_ID(N'[dbo].[Events_Message]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events_Message];
GO
IF OBJECT_ID(N'[dbo].[Events_Birthday]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events_Birthday];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Profiles'
CREATE TABLE [dbo].[Profiles] (
    [Id] nvarchar(450)  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [NikName] nvarchar(50)  NULL,
    [Avatar] varbinary(max)  NULL,
    [Email] nvarchar(50)  NULL,
    [IsMale] bit  NOT NULL,
    [UpdateTime] datetime  NOT NULL,
    [Thumbnail] varbinary(max)  NULL
);
GO

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [Id] nvarchar(450)  NOT NULL,
    [UserId] nvarchar(max)  NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Type] int  NOT NULL,
    [CreateDate] datetime  NOT NULL,
    [AddressOfRecord] nvarchar(max)  NOT NULL,
    [Status] int  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [MyProfile_Id] nvarchar(450)  NOT NULL,
    [Recipient_Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Conversations'
CREATE TABLE [dbo].[Conversations] (
    [Id] nvarchar(450)  NOT NULL,
    [CreateDate] datetime  NOT NULL,
    [IsPrivate] bit  NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [OwnerId] nvarchar(max)  NOT NULL,
    [MembersCount] int  NOT NULL,
    [Account_Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Recipients'
CREATE TABLE [dbo].[Recipients] (
    [Id] nvarchar(450)  NOT NULL,
    [ConversationId] nvarchar(max)  NOT NULL,
    [AccountId] nvarchar(max)  NOT NULL,
    [AccessType] int  NOT NULL,
    [Conversation_Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [Id] nvarchar(450)  NOT NULL,
    [OccurDate] datetime  NOT NULL,
    [Type] int  NOT NULL,
    [ConversationId] nvarchar(max)  NOT NULL,
    [ContentId] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [NumberOfSeen] int  NOT NULL,
    [Caption] nvarchar(max)  NULL,
    [Conversation_Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Contents'
CREATE TABLE [dbo].[Contents] (
    [Id] nvarchar(450)  NOT NULL,
    [Data] varbinary(max)  NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [Event_Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'ReverseContacts'
CREATE TABLE [dbo].[ReverseContacts] (
    [Id] nvarchar(450)  NOT NULL,
    [MemberId] nvarchar(max)  NOT NULL,
    [OwnerId] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Events_Call'
CREATE TABLE [dbo].[Events_Call] (
    [CallType] int  NOT NULL,
    [Duration] nvarchar(max)  NOT NULL,
    [CallerAccountId] nvarchar(max)  NOT NULL,
    [CalleeAcountId] varchar(max)  NOT NULL,
    [Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Events_Message'
CREATE TABLE [dbo].[Events_Message] (
    [SenderAccountId] nvarchar(max)  NOT NULL,
    [Text] nvarchar(max)  NULL,
    [Id] nvarchar(450)  NOT NULL
);
GO

-- Creating table 'Events_Birthday'
CREATE TABLE [dbo].[Events_Birthday] (
    [picture] varbinary(max)  NOT NULL,
    [WhoWasBornId] nvarchar(max)  NOT NULL,
    [BirthdayDate] datetime  NOT NULL,
    [Place] nvarchar(max)  NOT NULL,
    [Id] nvarchar(450)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Profiles'
ALTER TABLE [dbo].[Profiles]
ADD CONSTRAINT [PK_Profiles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Conversations'
ALTER TABLE [dbo].[Conversations]
ADD CONSTRAINT [PK_Conversations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Recipients'
ALTER TABLE [dbo].[Recipients]
ADD CONSTRAINT [PK_Recipients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Contents'
ALTER TABLE [dbo].[Contents]
ADD CONSTRAINT [PK_Contents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ReverseContacts'
ALTER TABLE [dbo].[ReverseContacts]
ADD CONSTRAINT [PK_ReverseContacts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events_Call'
ALTER TABLE [dbo].[Events_Call]
ADD CONSTRAINT [PK_Events_Call]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events_Message'
ALTER TABLE [dbo].[Events_Message]
ADD CONSTRAINT [PK_Events_Message]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events_Birthday'
ALTER TABLE [dbo].[Events_Birthday]
ADD CONSTRAINT [PK_Events_Birthday]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [MyProfile_Id] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_MyProfileAccountInfo]
    FOREIGN KEY ([MyProfile_Id])
    REFERENCES [dbo].[Profiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MyProfileAccountInfo'
CREATE INDEX [IX_FK_MyProfileAccountInfo]
ON [dbo].[Accounts]
    ([MyProfile_Id]);
GO

-- Creating foreign key on [Account_Id] in table 'Conversations'
ALTER TABLE [dbo].[Conversations]
ADD CONSTRAINT [FK_AccountConversation]
    FOREIGN KEY ([Account_Id])
    REFERENCES [dbo].[Accounts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountConversation'
CREATE INDEX [IX_FK_AccountConversation]
ON [dbo].[Conversations]
    ([Account_Id]);
GO

-- Creating foreign key on [Conversation_Id] in table 'Recipients'
ALTER TABLE [dbo].[Recipients]
ADD CONSTRAINT [FK_ConversationRecipient]
    FOREIGN KEY ([Conversation_Id])
    REFERENCES [dbo].[Conversations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConversationRecipient'
CREATE INDEX [IX_FK_ConversationRecipient]
ON [dbo].[Recipients]
    ([Conversation_Id]);
GO

-- Creating foreign key on [Recipient_Id] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_AccountRecipient]
    FOREIGN KEY ([Recipient_Id])
    REFERENCES [dbo].[Recipients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountRecipient'
CREATE INDEX [IX_FK_AccountRecipient]
ON [dbo].[Accounts]
    ([Recipient_Id]);
GO

-- Creating foreign key on [Conversation_Id] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_ConversationEvent]
    FOREIGN KEY ([Conversation_Id])
    REFERENCES [dbo].[Conversations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConversationEvent'
CREATE INDEX [IX_FK_ConversationEvent]
ON [dbo].[Events]
    ([Conversation_Id]);
GO

-- Creating foreign key on [Event_Id] in table 'Contents'
ALTER TABLE [dbo].[Contents]
ADD CONSTRAINT [FK_EventContent]
    FOREIGN KEY ([Event_Id])
    REFERENCES [dbo].[Events]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventContent'
CREATE INDEX [IX_FK_EventContent]
ON [dbo].[Contents]
    ([Event_Id]);
GO

-- Creating foreign key on [Id] in table 'Events_Call'
ALTER TABLE [dbo].[Events_Call]
ADD CONSTRAINT [FK_Call_inherits_Event]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Events]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Events_Message'
ALTER TABLE [dbo].[Events_Message]
ADD CONSTRAINT [FK_Message_inherits_Event]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Events]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Events_Birthday'
ALTER TABLE [dbo].[Events_Birthday]
ADD CONSTRAINT [FK_Birthday_inherits_Event]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Events]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------